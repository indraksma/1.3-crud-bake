<?php
App::uses('AppModel', 'Model');
/**
 * Mahasiswa Model
 *
 */
class Mahasiswa extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'mahasiswa';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idmhs';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'idmhs';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nama' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}

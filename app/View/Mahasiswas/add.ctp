<div class="mahasiswas form">
<?php echo $this->Form->create('Mahasiswa'); ?>
	<fieldset>
		<legend><?php echo __('Add Mahasiswa'); ?></legend>
	<?php
		echo $this->Form->input('nama');
		echo $this->Form->input('alamat');
		echo $this->Form->input('jk');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Mahasiswas'), array('action' => 'index')); ?></li>
	</ul>
</div>

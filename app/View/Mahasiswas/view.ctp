<div class="mahasiswas view">
<h2><?php echo __('Mahasiswa'); ?></h2>
	<dl>
		<dt><?php echo __('Idmhs'); ?></dt>
		<dd>
			<?php echo h($mahasiswa['Mahasiswa']['idmhs']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nama'); ?></dt>
		<dd>
			<?php echo h($mahasiswa['Mahasiswa']['nama']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Alamat'); ?></dt>
		<dd>
			<?php echo h($mahasiswa['Mahasiswa']['alamat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Jk'); ?></dt>
		<dd>
			<?php echo h($mahasiswa['Mahasiswa']['jk']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mahasiswa'), array('action' => 'edit', $mahasiswa['Mahasiswa']['idmhs'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mahasiswa'), array('action' => 'delete', $mahasiswa['Mahasiswa']['idmhs']), array('confirm' => __('Are you sure you want to delete # %s?', $mahasiswa['Mahasiswa']['idmhs']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Mahasiswas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mahasiswa'), array('action' => 'add')); ?> </li>
	</ul>
</div>

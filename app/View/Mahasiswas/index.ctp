<div class="mahasiswas index">
	<h2><?php echo __('Mahasiswas'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('idmhs'); ?></th>
			<th><?php echo $this->Paginator->sort('nama'); ?></th>
			<th><?php echo $this->Paginator->sort('alamat'); ?></th>
			<th><?php echo $this->Paginator->sort('jk'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($mahasiswas as $mahasiswa): ?>
	<tr>
		<td><?php echo h($mahasiswa['Mahasiswa']['idmhs']); ?>&nbsp;</td>
		<td><?php echo h($mahasiswa['Mahasiswa']['nama']); ?>&nbsp;</td>
		<td><?php echo h($mahasiswa['Mahasiswa']['alamat']); ?>&nbsp;</td>
		<td><?php echo h($mahasiswa['Mahasiswa']['jk']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $mahasiswa['Mahasiswa']['idmhs'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $mahasiswa['Mahasiswa']['idmhs'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mahasiswa['Mahasiswa']['idmhs']), array('confirm' => __('Are you sure you want to delete # %s?', $mahasiswa['Mahasiswa']['idmhs']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Mahasiswa'), array('action' => 'add')); ?></li>
	</ul>
</div>

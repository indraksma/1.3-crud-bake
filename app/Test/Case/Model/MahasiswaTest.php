<?php
App::uses('Mahasiswa', 'Model');

/**
 * Mahasiswa Test Case
 */
class MahasiswaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mahasiswa'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Mahasiswa = ClassRegistry::init('Mahasiswa');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Mahasiswa);

		parent::tearDown();
	}

}
